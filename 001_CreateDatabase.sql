CREATE DATABASE ModernWays;
-- Maak alleen een databank als er nog geen bestaat met dezelfde naam.
-- Dit is trouwens hoe je commentaar schrijft in MySQL.
CREATE DATABASE IF NOT EXISTS ModernWays;
USE ModernWays;