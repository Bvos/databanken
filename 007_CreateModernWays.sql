CREATE DATABASE ModernWays;
CREATE DATABASE IF NOT EXISTS ModernWays;
USE ModernWays;

CREATE TABLE Boeken(
	Voornaam varchar(50) CHAR SET utf8mb4,
    Familienaam VARCHAR(200) CHAR SET utf8mb4 NOT NULL,
    Titel VARCHAR(255) CHAR SET utf8mb4,
    Stad VARCHAR(50) CHAR SET utf8mb4,
    -- alleen het jaartal, geen datetime
    -- omdat de kleinste datum daarin 1753 is
    -- varchar omdat we ook jaartallen kleiner dan 1000 hebben
    Verschijningsjaar VARCHAR(4),
    Uitgeverij VARCHAR(80) CHAR SET utf8mb4,
    Herdruk VARCHAR(4),
    Commentaar VARCHAR(150) CHAR SET utf8mb4
    );
    
    